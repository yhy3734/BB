package com.eova.ai.DocParser;

import cn.bblocks.chatgpt.util.MilvusClientUtil2;
import cn.bblocks.chatgpt.util.RecursiveCharacterTextSplitter;
import cn.hutool.core.util.StrUtil;
import cn.bblocks.chatgpt.EmbeddingModel;
import com.google.common.collect.Lists;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhao
 * @date 2023年09月10日 下午3:23
 * @description
 */
public class PdfParse extends AbstractParser {
   // private static final int MAX_LENGTH = 200;

    public String getParserName(){
        return "pdf";
    }

    @Override
    public List<String> parse(InputStream inputStream) throws IOException {
        List<String> ans = new ArrayList<>();
        // 打开 PDF 文件
        try(PDDocument document = PDDocument.load(inputStream);){
            PDFTextStripper stripper = new PDFTextStripper();
            // 获取文本内容
            String text = stripper.getText(document);
            text = format(text);

            String[] sentence = text.split(AbstractParser.PART_SPLITTER);
            for (String s : sentence) {
                s = StrUtil.trim(s);
                if(StrUtil.isEmpty(s) )
                    continue;

                ans.add(s);
            }
        }finally {
            inputStream.close();
        }
        return ans;
    }

    public String format(String text){
        int p = -1;
        while( (p = text.indexOf("\r\n ")) >= 0){
            text = text.replaceAll("\r\n ", "\r\n");
        }

        return text;
    }
    public static void main(String[] args) throws Exception {
       // PdfParse txtParse = new PdfParse();
        AbstractParser txtParse = AbstractParser.getParserName("pdf");

        File file = new File("d:\\03ee8f55ad9f4b139bf1512dbe7e1b63.pdf");
        file = new File("d:\\臣仕智能FAQ.pdf");

        List<String> result = txtParse.parse(new FileInputStream(file));

        System.out.println("line size:"+result.size());

        int i = 0;
        for (String line : result) {
            System.out.println("line:"+(++i)+"========>"+line);
        }

        System.out.println("<============================>");
        List<String> parts = result;

        /**
         * 为了防止段落也超限，再进行分隔
         */
        RecursiveCharacterTextSplitter textSplitter = new RecursiveCharacterTextSplitter(null, MilvusClientUtil2.MAX_LENGTH/8, MilvusClientUtil2.MAX_LENGTH/80);

        List<String> contents = Lists.newArrayList();
        int max = textSplitter.getChunkSize();//1000
        int min = textSplitter.getChunkOverlap();//100

        StringBuilder last = new StringBuilder();
        for(String sentence:parts){
            if(last.length() != 0)
                last.append(LINE_SPLITTER);
            last.append(sentence);

            int length = last.length();
            if(length < min){//需要合并么？
                continue;
            }else if(length >= max){
                contents.addAll(textSplitter.splitText(last.toString()));
            }else{
                contents.add(last.toString());
            }
            last.setLength(0);
        }

        if(last.length() >0 )
            contents.add(last.toString());

        i = 0;
        for (String line : contents) {
            System.out.println("line:"+(++i)+"========>"+line);
        }
    }
}
