package com.eova.ai.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 训练模型类型
 *
 */
@Getter
public enum AiModelTrainType {
    /**
     *
     *  类型：
     *  1=录入问答，2=提交文件 3=内容提交
     */
    QUESTION(1,"问答"),
    FILE(2,"提交文件"),
    CONTENT(3,"提交内容"),

    ;
    public final Integer  type;

    public final String  name;

    private static final Map<Integer, AiModelTrainType> MAP = Arrays.stream(values())
            .collect(Collectors.toMap(item->item.type, item->item));

    AiModelTrainType(Integer type,  String  name){
        this.type = type;
        this.name = name;
    }

    public static AiModelTrainType get(Integer type){
        return MAP.get(type);
    }
}
