package com.eova.ai.cache;

import cn.bblocks.chatgpt.cache.ICache;
import cn.hutool.core.util.StrUtil;
import com.eova.config.EovaConfig;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>Project: bb-project - CacheRedis</p>
 * <p>描述：gpt向量相关的查询缓存</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/11 15:29
 * @Version 1.0
 * @since 8
 */
@Slf4j
public class CacheRedis implements ICache {

    /**
     * 默认过期时间（s）
     */
    private long defaultExpire = 60;
    /**
     * 模块过期时间（s）
     */
    private Map<String,Long> expires ;

    public CacheRedis(@NonNull Map<String,Long> expires, long defaultExpire){
        this.expires = new HashMap(expires);
        this.defaultExpire = defaultExpire;
    }

    public void put(String cacheName, String key, Object value) {
        Cache cache = Redis.use();
        if(cache == null)
            return;

        cacheName = getCacheNameIfNullWithDefault(cacheName);
        Long expire = expires.get(cacheName);
        if(expire == null)
            expire = defaultExpire;

        cache.setex(EovaConfig.SYS_MODULE+"::"+cacheName+"::"+key,expire,value);
    }


    @Override
    public <T> T get(String cacheName, String key) {
        Cache cache = Redis.use();
        if(cache == null)
            return null;
        T o = cache.get("bb::"+cacheName+"::"+key);
        //log.info("CacheRedis get {}:{} " ,key,o);
        return o;
    }


    private String getCacheNameIfNullWithDefault(String cacheName){
        if(StrUtil.isEmpty(cacheName))
            return ICache.CACHE_NAME_DEFAULT;
        else
            return cacheName;
    }
}
