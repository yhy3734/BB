package com.eova.ai;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.eova.ai.listener.WebSocketStreamListener;
import com.eova.ai.vo.ChatSession;
import com.eova.common.Response;
import com.eova.common.utils.xx;
import com.eova.config.EovaConst;
import com.eova.exception.BusinessException;
import com.eova.model.*;
import com.eova.service.sm;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.jfinal.core.Controller;
import cn.bblocks.chatgpt.EmbeddingModel;
import cn.bblocks.chatgpt.db.VectorsDb;
import cn.bblocks.chatgpt.entity.chat.ChatCompletion;
import cn.bblocks.chatgpt.entity.chat.ChatCompletionResponse;
import cn.bblocks.chatgpt.entity.chat.Message;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>Project: bb-project - AiChatController</p>
 * <p>描述：ai机器人相关操作-支持响应聊天/同步聊天</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/6 9:38
 * @Version 1.0
 * @since 8
 */
@Slf4j
public class AiChatController extends Controller {

    /**
     * 聊天首页
     * //toIndex/1?authorizeCode=xx&force=1&name=自由对话
     */
    public void toIndex() {
        Long robotId = this.getParaToLong(0);
        String authorizeCode = this.get("authorizeCode");
        Integer force = this.getInt("force");
        String name = this.get("name");
        if(robotId == null || robotId<=0){
            throw new BusinessException("参数异常！");
           /* this.setAttr("ERROR",e.getMsg());
            xx.render(this,"/chatgpt/index.html");
            return;*/
        }

        this.setAttr("robotId",robotId);
        this.setAttr("authorizeCode",authorizeCode);
        this.setAttr("force",force);
        this.setAttr("name",name);
        this.setAttr("token",StpUtil.getTokenValue());

        ChatSession currentSession = (ChatSession)StpUtil.getSession().get(ChatSession.CHAT_KEY);
        if(currentSession != null && currentSession.getRobot().getLong("id").equals(robotId)) {//存在
            this.setAttr(ChatSession.CHAT_KEY,currentSession);
        }else{
            try{
                User user = getAttr(EovaConst.USER);
                currentSession = sm.chatService.createChat(robotId,authorizeCode,name,force,user);

                this.setAttr(ChatSession.CHAT_KEY,currentSession);
                StpUtil.getSession().set(ChatSession.CHAT_KEY, currentSession);
            }catch (BusinessException e) {
                this.setAttr("ERROR",e.getMsg());
            }
        }

        xx.render(this,"/chatgpt/index.html");
    }


    /**
     * 获取当前会话
     */
    public void getCurrentSesion(){
        ChatSession currentSession = (ChatSession)StpUtil.getSession().get(ChatSession.CHAT_KEY);
        renderJson(Response.sucData(currentSession));
    }


    /**
     * 当前会话明细（参数 limit 限制数量）
     */
    public void getCurrentSesionRecords(){
        Integer limit = this.getInt("limit",100);

        ChatSession currentSession = (ChatSession)StpUtil.getSession().get(ChatSession.CHAT_KEY);
        Long sessionId = currentSession.getSessionId();

        List<AiChatSessionRecord> records = AiChatSessionRecord.dao.queryAllRecords(sessionId,limit);
        renderJson(Response.sucData(records));
    }


    /**
     * 同步聊天，返回响应
     * 入参：sessionId，question
     */
    public void gptChat() {
        User user = getAttr(EovaConst.USER);
        Long sessionId = this.getLong("sessionId");
        String question = this.get("question");
        if (StrUtil.isEmpty(question) || sessionId == null) {
            renderJson(Response.err("参错错误！"));
            return;
        }
        ChatSession chatSession = (ChatSession)StpUtil.getSession().get(ChatSession.CHAT_KEY);
        if (chatSession == null || !chatSession.getSessionId().equals(sessionId)) {
            renderJson(Response.err("无此会话"));
            return;
        }

        ChatCompletionResponse completionResponse = sm.chatService.chatByChatSession(chatSession,question);
        renderJson(Response.sucData(completionResponse));
        return;
    }

    /**
     * 异步聊天，基于ws推送结果
     */
    public void gptChatStream() {
        User user = getAttr(EovaConst.USER);
        Long sessionId = this.getLong("sessionId");
        String question = this.get("question");
        if (StrUtil.isEmpty(question) || sessionId == null) {
            renderJson(Response.err("参错错误！"));
            return;
        }
        ChatSession chatSession = (ChatSession)StpUtil.getSession().get(ChatSession.CHAT_KEY);
        if (chatSession == null || !chatSession.getSessionId().equals(sessionId)) {
            renderJson(Response.err("无此会话"));
            return;
        }

        String theCollectionName = chatSession.getCollectionName();
        List<Integer> modelIds = null;
        if(theCollectionName != null)
           modelIds = chatSession.getRebotModels().stream().map(m->m.getLong("id").intValue()).collect(Collectors.toList());

        AiChatSession session = chatSession.getAiChatSession();
        AiRobot robot = chatSession.getRobot();

        /**
         * 设置系统设置
         */
        String templateDesc = StrUtil.trim(robot.getStr("template_desc"));

        List<Message> messages = Lists.newArrayList();
        if (!xx.isEmpty(templateDesc))
            messages.add(Message.ofSystem(templateDesc));
        messages.add(Message.of(question));

        ChatCompletion chatCompletion = ChatCompletion.builder()
                .messages(messages)
                .withCollectionName(theCollectionName)
                .modelIds(modelIds)
                .build();

        WebSocketStreamListener listener = new WebSocketStreamListener(StpUtil.getTokenValue());
        EmbeddingModel embeddingModel = sm.chatService.getTheEmbeddingModel(session,robot);
        embeddingModel.streamChatAllCompletion(chatCompletion,listener);
        renderJson(Response.suc("完成"));
    }

    /**
     * 创建会话（基于机器人）,并把会话信息绑定到此token，退出后将无效
     * 如果存在会话，需要强制参数：force=1，否则将返回已存在会话
     */
    public void create() {
        Long robotId = this.getLong("robotId");
        String authorizeCode = this.get("authorizeCode");
        String name = this.get("name");
        Integer force = this.getInt("force",0);

        ChatSession chatSession = null;
        try {
            User user = getAttr(EovaConst.USER);
            chatSession = sm.chatService.createChat(robotId, authorizeCode, name, force,user);
        }catch (BusinessException e) {
            renderJson(Response.err(e.getMsg()));
            return;
        }
        StpUtil.getSession().set(ChatSession.CHAT_KEY, chatSession);
        renderJson(Response.sucData(chatSession));
    }

    /**
     * 退出会话
     */
    public void outChat(){
        ChatSession chatSession = (ChatSession)StpUtil.getSession().get(ChatSession.CHAT_KEY);
        if(chatSession != null){
            StpUtil.getSession().delete(ChatSession.CHAT_KEY);
            Long sessionId = chatSession.getSessionId();
            AiChatSession.dao.updateOver(sessionId);
        }
        renderJson(Response.suc("退出完成"));
    }



    /**
     * 创建会话（基于机器人）,并把会话信息绑定到此token，退出后将无效
     * 如果存在会话，需要强制参数：force=1，否则将返回已存在会话
     */
    /*private ChatSession create(@NonNull Long robotId, String authorizeCode, String name, Integer force) {
        User user = getAttr(EovaConst.USER);
        AiRobot robot = AiRobot.dao.getValidRobot(robotId);
        if (robot == null) {
            //renderJson(Response.err("无效机器人"));
            throw new BusinessException("无效机器人");
        } else {//非自己且不让外部使用的，也无效
            Long userId = robot.getLong("user_id");
            Integer type = robot.getInt("type");//类型：1=私有,2=公开,3=授权访问
            String authorizeCodeDb = robot.getStr("authorize_code");
            if (type.equals(1)) {
                if (!userId.equals(user.getId().longValue())) {
                   // renderJson(Response.err("私有机器人禁止访问！"));
                    throw new BusinessException("私有机器人禁止访问");
                }
            } else if (type.equals(3)) {
                if (!userId.equals(user.getId().longValue()) &&
                        (StrUtil.isEmpty(authorizeCode) || !ObjectUtil.equals(authorizeCodeDb, authorizeCode))) {
                   // renderJson(Response.err("无授权码or授权码校验失败！"));
                    throw new BusinessException("无授权码or授权码校验失败！");
                }
            }
        }

        AiChatSession session = new AiChatSession();
        session.set("user_id", user.getId().longValue());
        session.set("create_time", new Date());
        session.set("robot_id", robot.getLong("id"));
        session.set("key_type", robot.getInt("key_type"));
        session.set("name", StrUtil.isNotEmpty(name) ? name : "不知名会话");
        session.set("type", 2);
        session.save();

        ChatSession chatSession = (ChatSession)StpUtil.getSession().get(ChatSession.CHAT_KEY);
        if(!Integer.valueOf(1).equals(force)) {
            if (chatSession != null) {
                //renderJson(Response.err("已存在会话！"));
                throw new BusinessException("已存在会话！");
            }
        }

        if(chatSession != null)
            log.info("存在会话：{},将踢出新建新会话！",chatSession.getAiChatSession().getLong("id"));

        String modelIds = robot.getStr("model_ids");

        String collectionName = null;
        List<AiModel> models = null;
        if(StrUtil.isNotEmpty(modelIds)) {
            models = AiModel.dao.queryModelByIds(Splitter.on(",").splitToList(modelIds));
            if(!models.isEmpty())
                collectionName = VectorsDb.COLLECTTION_NAME_DEFAULT;
        }

        chatSession = ChatSession
                .builder()
                .sessionId(session.getLong("id"))
                .aiChatSession(session)
                .robot(robot)
                .collectionName(collectionName)
                .rebotModels(models)
                .build();
        //StpUtil.getSession().set(ChatSession.CHAT_KEY,chatSession);
        //renderJson(Response.sucData(chatSession));
        return chatSession;
    }*/
}
