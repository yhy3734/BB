package com.eova.ai.handler;


import com.eova.ai.DocParser.AbstractParser;
import com.eova.model.AiModel;
import com.eova.model.AiRobot;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 训练接口
 */
public interface ModeTrainHandler {


    /**
     *
     * @param aiModel
     * @param robot
     * @return 1=成功，0=失败
     */
    int train(AiModel aiModel, AiRobot robot );

    public abstract Integer getTrainType();


    /**
     * 初始化的处理器
     */
    static Map<Integer,ModeTrainHandler> parsers = allHandler();;

    /**
     * 根据添加类型获取处理器
     * @param addType
     * @return 可能为空
     */
    public static ModeTrainHandler getTrainHandler(Integer addType){
        return parsers.get(addType);
    }

     static Map<Integer,ModeTrainHandler> allHandler(){
        // 获取当前类的Class对象
        Class<?> clazz = ModeTrainHandler.class;
        // 获取当前类的包名
        Package pkg = clazz.getPackage();
        String packageName = pkg.getName();

        Set<Class<? extends ModeTrainHandler>> subTypes = childClass();
        Map<Integer,ModeTrainHandler> result = new HashMap<Integer,ModeTrainHandler>();

        for(Class<? extends ModeTrainHandler> one:subTypes){
            try {
                // one.
                //  Class catClass = Class.forName(one.getName());
                // 实例化这个类
                ModeTrainHandler handler = (ModeTrainHandler) one.newInstance();
                result.put(handler.getTrainType(), handler);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return result;
    }

    static Set<Class<? extends ModeTrainHandler>> childClass(){
        // 获取当前类的Class对象
        Class<?> clazz = ModeTrainHandler.class;
        // 获取当前类的包名
        Package pkg = clazz.getPackage();
        String packageName = pkg.getName();

        Reflections reflections = new Reflections(packageName+".impl");
        Set<Class<? extends ModeTrainHandler>> subTypes = reflections.getSubTypesOf(ModeTrainHandler.class);
        // subTypes.forEach(x -> System.out.println(x));
        return subTypes;
    }
}
