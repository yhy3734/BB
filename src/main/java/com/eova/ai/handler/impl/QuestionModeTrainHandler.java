package com.eova.ai.handler.impl;

import com.eova.ai.DocParser.AbstractParser;
import com.eova.ai.enums.AiModelTrainType;
import com.eova.ai.handler.ModeTrainHandler;
import com.eova.model.AiModel;
import com.eova.model.AiRobot;
import com.eova.service.sm;
import com.google.common.collect.Lists;

import cn.bblocks.chatgpt.ChatGPT;
import cn.bblocks.chatgpt.ChatGPTStream;
import cn.bblocks.chatgpt.EmbeddingModel;
import cn.bblocks.chatgpt.entity.chat.DataSqlEntity;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
public class QuestionModeTrainHandler implements ModeTrainHandler {

    public Integer getTrainType(){
        return AiModelTrainType.QUESTION.getType();
    }

    /**
     * 训练
     * @param aiModel
     * @return 1=成功，0=失败
     */
    @Override
    public int train(AiModel aiModel, AiRobot robot ){
        String question = aiModel.getStr("file_name");
        String answer = aiModel.getStr("content");
        ChatGPT chatGpt = sm.chatService.getTheGpt(robot);
        ChatGPTStream gptStream = sm.chatService.getTheStreamGpt(chatGpt);
        EmbeddingModel embeddingModel = sm.chatService.getTheEmbeddingModel(chatGpt,gptStream);
        try {
            List<String> parts = Lists.newArrayList(question + AbstractParser.LINE_SPLITTER + answer);

            DataSqlEntity params = new DataSqlEntity();
            params.setModelId(aiModel.getLong("id").intValue());
            params.setRobotId(robot.getLong("id").intValue());
            embeddingModel.saveModel(parts,null,params);
        }catch (Exception e){
            log.error("训练模型：{} 异常：",aiModel.getLong("id"),e);
            return 0;
        }
        return 1;
    }
}
