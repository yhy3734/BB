/**
 * 
 */
package com.eova.ai;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.eova.common.utils.xx;
import com.eova.config.EovaConst;
import com.eova.exception.BusinessException;
import com.eova.model.User;
import com.eova.service.sm;
import com.google.common.base.Splitter;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import cn.bblocks.chatgpt.entity.chat.Message;
import lombok.extern.slf4j.Slf4j;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

/**
 * @Description: 主要用来推送聊天流消息,以token为单位推送
 * @author Zhao
 * @date 2023-09-08
 * @version v1.0
 */
@Slf4j
@ServerEndpoint("/aichat.ws/{token}")
public class AiChatWebSocket {
	public static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";

	/**
	 * 全局会话
	 */
	private static Map<String, Session> clients = new ConcurrentHashMap<String, Session>();

	//这里用来接收客户端发送的消息
	@OnMessage
	public void message(String message, Session session) {
		log.info("Message: {}" , message);
	}
	//当连接关闭时会调用这个方法
	@OnClose
	public void close(Session session) {
		dropClient(session);
	}
	//当连接建立时会调用这个方法
	@OnOpen
	public void open(@PathParam("token") String token, Session session
			,EndpointConfig config) {
		User user = null;
		Object loginId =StpUtil.getLoginIdByToken(token);
		user = (User)StpUtil.getSessionByLoginId(loginId).getByDevice(EovaConst.USER);

		log.info("session:{}",user!=null?user.getId():null);

		if(user != null){
			log.debug("真的连接上来：{}(userId:{})",token,user.getId());
			session.getUserProperties().put(USER_TOKEN_KEY,token);

			clients.put(token, session);
		}else{
			session.getUserProperties().remove(USER_TOKEN_KEY);
		}
	}
	//当发生错误时调用这个方法
	@OnError
	public void error(Session session,Throwable throwable) {
		dropClient(session);
	}

	/**
	 * 关闭客户端存储
	 * @param session
	 */
	private void dropClient(Session session){
		String key = null;
		if(session != null && session.getUserProperties().get(USER_TOKEN_KEY) != null){
			key = (String)session.getUserProperties().get(USER_TOKEN_KEY);
			clients.remove(key);
		}
	}


	/**
	 * 收到信息
	 * @param token
	 * @param messages
	 * @throws IOException
	 */
	public static void OnMessage(String token,Message messages) throws IOException {
		String jsonStr = JSONUtil.toJsonStr(messages);

		sendMessage(token,jsonStr);
	}

	public static void sendMessage(String token,String message) throws IOException {
		Session session = clients.get(token);

		if(session != null && session.isOpen()) {
			log.debug("发送消息给ID:{}的信息:{},session:{}", token, message, session.getId());
			session.getBasicRemote().sendText(message);
		}else{
			log.debug("用户：{} 未连接上来，放弃本次消息！",token);
		}
	}
}
