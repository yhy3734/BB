package com.eova.service;

import cn.bblocks.chatgpt.entity.chat.ChatCompletion;
import cn.bblocks.chatgpt.entity.chat.ChatCompletionResponse;
import cn.bblocks.chatgpt.entity.chat.Message;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.eova.ai.cache.CacheRedis;
import com.eova.ai.handler.ModeTrainHandler;
import com.eova.ai.vo.ChatSession;
import com.eova.cache.CacheNone;
import com.eova.common.Response;
import com.eova.common.base.BaseService;
import com.eova.common.utils.xx;
import com.eova.config.EovaConfig;
import com.eova.config.EovaConst;
import com.eova.exception.BusinessException;
import com.eova.model.*;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import cn.bblocks.chatgpt.ChatGPT;
import cn.bblocks.chatgpt.ChatGPTStream;
import cn.bblocks.chatgpt.EmbeddingModel;
import cn.bblocks.chatgpt.cache.ICache;
import cn.bblocks.chatgpt.db.VectorsDb;
import cn.bblocks.chatgpt.db.impl.MilvusVectorsDb;
import cn.bblocks.chatgpt.db.impl.VectorsDbNone;
import cn.bblocks.chatgpt.util.AutoThreadPools;
import cn.bblocks.chatgpt.util.MilvusClientUtil2;
import cn.bblocks.chatgpt.util.Proxys;
import com.jfinal.plugin.redis.Redis;
import com.qiniu.common.Zone;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import io.milvus.client.MilvusServiceClient;
import io.milvus.param.ConnectParam;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.net.Proxy;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>Project: bb-project - ChatService</p>
 * <p>描述： ai chat api</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/6 12:37
 * @Version 1.0
 * @since 8
 */
@Slf4j
public class ChatService extends BaseService {
    public static final String MODULE = "CHAT";

    private MilvusServiceClient milvusClient;
    private MilvusClientUtil2 milvusClientUtil;

    /**
     * gpt相关缓存
     */
    private ICache cache;

    private VectorsDb vectorsDb ;
    public ChatService(){
        if(xx.isEmpty(xx.getConfig("ai_milvus_host"))){
            vectorsDb = VectorsDbNone.builder().build();
            log.info("未配置向量库，gpt启用直连模式！");
        }else {
            ConnectParam.Builder builder = ConnectParam.newBuilder()
                    .withHost(xx.getConfig("ai_milvus_host"))
                    .withPort( xx.getConfigInt("ai_milvus_port",9530));
            if(!xx.isEmpty(xx.getConfig("ai_milvus_user")))
                builder.withAuthorization(xx.getConfig("ai_milvus_user"),xx.getConfig("ai_milvus_psw"));

            ConnectParam connectParam = builder.build();
            milvusClient =  new MilvusServiceClient(connectParam);
            milvusClientUtil = new MilvusClientUtil2(milvusClient);

            vectorsDb = MilvusVectorsDb.builder()
                    .milvusClient(milvusClient)
                    .milvusClientUtil(milvusClientUtil)
                    .build();
            log.info("启动 ChatService 尤其是向量库：milvus 完成！");
        }


            Map<String,Long> expires = new HashMap<String, Long>();
            expires.put("Embedding",24*60*60L*7);//7天
            expires.put("ScoreDoc",1*60L);
            cache = new CacheRedis(expires,60L);

        //EmbeddingModel.LOCAL_NO_ANSWER_PRE = "抱歉，";
    }

    /**
     * 查询是否支持本地知识库
     * @return
     */
    public boolean suportModel(){
        if(vectorsDb == null || vectorsDb instanceof VectorsDbNone){
            return false;
        }else
            return true;
    }

    public EmbeddingModel getTheEmbeddingModel(@NonNull AiChatSession session ,@NonNull AiRobot robot){
        ChatGPT gpt = this.getTheGpt(session,robot);
        ChatGPTStream streamGpt = this.getTheStreamGpt(gpt);

        EmbeddingModel embeddingModel = this.getTheEmbeddingModel(gpt,streamGpt);
        return embeddingModel;
    }

    public EmbeddingModel getTheEmbeddingModel(ChatGPT chatGPT, ChatGPTStream chatGPTStream){
        EmbeddingModel embeddingModel = EmbeddingModel
                .builder()
                .vectorsDb(vectorsDb)
                .gpt(chatGPT)
                .streamGpt(chatGPTStream)
                .autoThreadPools(AutoThreadPools.getInstance())
                .scoreLimit(0.8f)
                .cache(cache)
                .defaultContextNum(3)//只提取三段吧（最大1000*3 =3000字）
                .build();
        return embeddingModel;
    }


    public ChatGPT getTheGpt(@NonNull AiChatSession session,@NonNull AiRobot robot){
        Long robotOwnerId = robot.getLong("user_id");

        int sessionkeyType = session.getNumber("key_type").intValue();//秘钥类型：1=私有，2=平台
        if(sessionkeyType == 1){//私有
            return getPrivateGpt(robotOwnerId);
        }else{//平台
            return getPlatformGpt();
        }
    }

    public ChatGPT getTheGpt(@NonNull AiRobot robot){
        Long robotOwnerId = robot.getLong("user_id");

        int sessionkeyType = robot.getNumber("key_type").intValue();//秘钥类型：1=私有，2=平台
        if(sessionkeyType == 1){//私有
            return getPrivateGpt(robotOwnerId);
        }else{//平台
            return getPlatformGpt();
        }
    }

    public ChatGPTStream getTheStreamGpt(@NonNull ChatGPT chatGPT ){
        ChatGPTStream chatGPTStream = ChatGPTStream.builder()
                .apiKeyList(chatGPT.getApiKeyList())
                .proxy(chatGPT.getProxy())
                .timeout(600)
                .apiHost(chatGPT.getApiHost())
                .build()
                .init();
        return chatGPTStream;
    }


    public  void timeTrans(int limit ){
        List<AiModel> models =  AiModel.dao.getToTrainModel(limit);
        if(CollUtil.isNotEmpty(models))
            log.info("查询到待训练模型数量：{}",models.size());
        models.forEach(m->{
            doModlesTrainAsync(m.getLong("id"));
        });
    }

    /**
     * 训练模型（确保关联了机器人）
     * @param aiModeId
     */
    public void doModlesTrainAsync(Long aiModeId) {
        log.info("提交模型训练(异步线程)：{}",aiModeId);

        Runnable runnable = ()->{
            AiModel aiModel = AiModel.dao.getByCache(aiModeId);
            if(aiModel == null || aiModel.getInt("state") .equals(2)){
                log.warn("模型：{} 状态或者数据存在问题，不予训练！");
            }else{
                log.info("开始模型训练：{}，状态标记为：训练中！",aiModeId);
                aiModel.set("state",1);
                aiModel.set("update_time",new Date());
                aiModel.update();

                ModeTrainHandler handler = ModeTrainHandler.getTrainHandler(aiModel.getInt("add_type"));
                if(handler == null){
                    log.warn("模型：{} 属于添加类型：{} ，未有模型训练处理方式！",aiModel.getInt("id"),aiModel.getInt("add_type"));

                    aiModel.set("state",3);
                    aiModel.set("remark","训练失败,不支持的添加类型！");
                    aiModel.set("update_time",new Date());
                    aiModel.update();
                }else{
                    AiRobot robot = AiRobot.dao.getValidModelRobot(aiModeId);
                    if (robot == null){
                        log.warn("模型：{} 未关联有效机器人，不予训练！");
                        return;
                    }
                    int ret =  handler.train(aiModel,robot);
                    log.info("模型：{} 训练结果：{}",aiModel.getInt("id"),ret);

                    if(ret == 1){
                        aiModel.set("state",2);
                        aiModel.set("remark","完成！");
                    }else{
                        if(StrUtil.isNotEmpty(aiModel.getStr("remark"))){
                            aiModel.set("remark",StrUtil.maxLength(aiModel.getStr("remark"),40));
                        }else{
                            aiModel.set("remark","训练失败！");
                        }
                        aiModel.set("state",3);
                    }
                    aiModel.set("update_time",new Date());
                    aiModel.update();
                }
            }

        };
        ThreadUtil.execAsync(runnable,true);
    }


    /**
     * 获取微信会话（为空会创建）
     * @param robotId
     * @return
     */
    public ChatSession getWxChat(@NonNull Long robotId) {
        String key = EovaConfig.SYS_MODULE +"::"+ MODULE + "::wx"+ "::"+robotId;
        ChatSession chatSession = Redis.use().get(key);
        if(chatSession != null){
            return chatSession;
        }else{
            chatSession = sm.chatService.createWxChat(robotId);
            Redis.use().setex(key,120L,chatSession);//120秒缓存周期
            return chatSession;
        }
    }

    /**
     * 创建wx会话（存在则为为查询的数据）
     * @param robotId
     * @return
     */
    public ChatSession createWxChat(@NonNull Long robotId) {
        AiRobot robot = AiRobot.dao.getValidRobot(robotId);

        AiChatSession aiChatSession = AiChatSession.dao.getWxChatSession(robotId);
        if(aiChatSession == null){
            aiChatSession = createWxChatSession(robotId);
        }

        String modelIds = robot.getStr("model_ids");
        String collectionName = null;
        List<AiModel> models = null;
        if(StrUtil.isNotEmpty(modelIds)) {
            models = AiModel.dao.queryModelByIds(Splitter.on(",").splitToList(modelIds));
            if(!models.isEmpty())
                collectionName = VectorsDb.COLLECTTION_NAME_DEFAULT;
        }

        ChatSession chatSession = ChatSession
                .builder()
                .sessionId(aiChatSession.getLong("id"))
                .aiChatSession(aiChatSession)
                .robot(robot)
                .collectionName(collectionName)
                .rebotModels(models)
                .build();
        return chatSession;
    }

    private AiChatSession createWxChatSession(@NonNull Long robotId) {
        AiRobot robot = AiRobot.dao.getValidRobot(robotId);
        if (robot == null || Integer.valueOf(1).equals(robot.getInt("qywx_state")) ) {
            //renderJson(Response.err("无效机器人"));
            throw new BusinessException("无效机器人");
        }

        AiChatSession session = new AiChatSession();
        //session.set("user_id", cuser.getId().longValue());
        session.set("create_time", new Date());
        session.set("robot_id", robot.getLong("id"));
        session.set("key_type", robot.getInt("key_type"));
        session.set("name", robot.getStr("qywx_name"));
        session.set("type", 1);
        session.save();

        return session;
    }

    /**
     * 创建会话（）
     * @param robotId
     * @param authorizeCode
     * @param name
     * @param force
     * @param cuser
     * @return
     */
    public ChatSession createChat(@NonNull Long robotId, String authorizeCode, String name, Integer force
            ,User cuser) {
        AiRobot robot = AiRobot.dao.getValidRobot(robotId);
        if (robot == null) {
            //renderJson(Response.err("无效机器人"));
            throw new BusinessException("无效机器人");
        } else {//非自己且不让外部使用的，也无效
            Long userId = robot.getLong("user_id");
            Integer type = robot.getInt("type");//类型：1=私有,2=公开,3=授权访问
            String authorizeCodeDb = robot.getStr("authorize_code");
            if (type.equals(1)) {
                if (!userId.equals(cuser.getId().longValue())) {
                    // renderJson(Response.err("私有机器人禁止访问！"));
                    throw new BusinessException("私有机器人禁止访问");
                }
            } else if (type.equals(3)) {
                if (!userId.equals(cuser.getId().longValue()) &&
                        (StrUtil.isEmpty(authorizeCode) || !ObjectUtil.equals(authorizeCodeDb, authorizeCode))) {
                    // renderJson(Response.err("无授权码or授权码校验失败！"));
                    throw new BusinessException("无授权码or授权码校验失败！");
                }
            }
        }

        AiChatSession session = new AiChatSession();
        session.set("user_id", cuser.getId().longValue());
        session.set("create_time", new Date());
        session.set("robot_id", robot.getLong("id"));
        session.set("key_type", robot.getInt("key_type"));
        session.set("name", StrUtil.isNotEmpty(name) ? name : "不知名会话");
        session.set("type", 2);
        session.save();

        ChatSession chatSession = (ChatSession) StpUtil.getSession().get(ChatSession.CHAT_KEY);
        if(!Integer.valueOf(1).equals(force)) {
            if (chatSession != null) {
                //renderJson(Response.err("已存在会话！"));
                throw new BusinessException("已存在会话！");
            }
        }

        if(chatSession != null)
            log.info("存在会话：{},将踢出新建新会话！",chatSession.getAiChatSession().getLong("id"));

        String modelIds = robot.getStr("model_ids");

        String collectionName = null;
        List<AiModel> models = null;
        if(StrUtil.isNotEmpty(modelIds)) {
            models = AiModel.dao.queryModelByIds(Splitter.on(",").splitToList(modelIds));
            if(!models.isEmpty())
                collectionName = VectorsDb.COLLECTTION_NAME_DEFAULT;
        }

        chatSession = ChatSession
                .builder()
                .sessionId(session.getLong("id"))
                .aiChatSession(session)
                .robot(robot)
                .collectionName(collectionName)
                .rebotModels(models)
                .build();
        //StpUtil.getSession().set(ChatSession.CHAT_KEY,chatSession);
        //renderJson(Response.sucData(chatSession));
        return chatSession;
    }


    /**
     * 同步会话
     * @param chatSession
     * @param question
     * @return
     */
    public ChatCompletionResponse chatByChatSession(ChatSession chatSession,String question){
        String collectionName = chatSession.getCollectionName();

        AiChatSession session = chatSession.getAiChatSession();
        AiRobot robot = chatSession.getRobot();
        EmbeddingModel embeddingModel = sm.chatService.getTheEmbeddingModel(session,robot);
        ChatCompletionResponse completionResponse = null;
        if (sm.chatService.suportModel() && StrUtil.isNotEmpty(collectionName) ) {
            List<Integer> modelIds = chatSession.getRebotModels().stream().map(m->m.getLong("id").intValue()).collect(Collectors.toList());
            completionResponse = embeddingModel.chatWithModel(question, collectionName,modelIds);
        }

        if (completionResponse != null) {
            String answer = completionResponse.getFinalContent();
            if (xx.isEmpty(answer) || answer.startsWith(EmbeddingModel.LOCAL_NO_ANSWER_PRE)) {
                log.info("本地模型知识库未能回复问题，即将使用网络知识库！");
                completionResponse = null;
            }
        }

        if (completionResponse != null)
            return completionResponse;

        /**
         * 设置系统设置
         */
        String templateDesc = StrUtil.trim(robot.getStr("template_desc"));

        List<Message> messages = Lists.newArrayList();
        if (!xx.isEmpty(templateDesc))
            messages.add(Message.ofSystem(templateDesc));
        messages.add(Message.of(question));

        ChatCompletion chatCompletion = ChatCompletion.builder()
                .model(ChatCompletion.Model.GPT_3_5_TURBO.getName())
                .messages(messages)
                .maxTokens(3000)
                .temperature(0.9)
                .build();
        completionResponse = embeddingModel.getGpt().chatCompletion(chatCompletion);
        return completionResponse;
    }


    private ChatGPT getPrivateGpt(Long robotOwnerId){
        Proxy proxy = null;
        List<String> apiKeyList = null;
        String host = null;

        List<AiUserKeys> keys = AiUserKeys.dao.queryUserKeys(robotOwnerId);
        if(CollUtil.isEmpty(keys)){
            log.warn("用户：{} 未配置秘钥！",robotOwnerId);
            throw new BusinessException("此机器人未配置秘钥");
        }
        AiUserKeys one = RandomUtil.randomEle(keys);
        host = one.getStr("host");
        apiKeyList = Lists.newArrayList(one.getStr("key"));
        if(!xx.isEmpty(one.getStr("proxy_ip")) && !xx.isEmpty(one.getInt("proxy_port"))){
            proxy =  Proxys.http(one.getStr("proxy_ip"), Integer.parseInt("proxy_port"));
        }
        if(proxy == null)
            proxy = Proxy.NO_PROXY;

        ChatGPT chatGPT = ChatGPT.builder()
                .apiKeyList(apiKeyList)
                .timeout(900)
                .proxy(proxy)
                .apiHost(host) //代理地址,https://api.openai.com/=》https://api.f2gpt.com
                .build()
                .init();

        return chatGPT;
    }

    private ChatGPT getPlatformGpt(){
        Proxy proxy = null;
        List<String> apiKeyList = null;
        String host = null;

        List<SysParams> sysParams = SysParams.dao.queryByGroup(SysParams.AI_GROUP);
        Optional<SysParams> hostUrl = sysParams
                .stream()
                .filter(p->"ai_host".equalsIgnoreCase(p.get("code")))
                .findAny();
        if(hostUrl.isPresent())
            host = hostUrl.get().getStr("value");


        Optional<SysParams> keys = sysParams
                .stream()
                .filter(p->"ai_keys".equalsIgnoreCase(p.get("code")))
                .findAny();

        if (!keys.isPresent() || xx.isEmpty(keys.get().getStr("value"))) {
            log.warn("平台无aikey！见bb_sys_params.ai_keys");
            throw new BusinessException("平台无aikey");
        }
        apiKeyList = Splitter.on(",").splitToList(keys.get().getStr("value"));

        Optional<SysParams> proxyIp = sysParams
                .stream()
                .filter(p->"ai_proxy_ip".equalsIgnoreCase(p.get("code")))
                .findAny();

        Optional<SysParams> proxyPort = sysParams
                .stream()
                .filter(p->"ai_proxy_port".equalsIgnoreCase(p.get("code")))
                .findAny();
        if (proxyIp.isPresent() && proxyPort.isPresent()){
            String ip =  proxyIp.get().getStr("value");
            String port =  proxyPort.get().getStr("value");
            if(!xx.isEmpty(ip) && !xx.isEmpty(port)){
                proxy =  Proxys.http(ip, Integer.parseInt(port));
            }
        }

        if(proxy == null)
            proxy = Proxy.NO_PROXY;

        ChatGPT chatGPT = ChatGPT.builder()
                .apiKeyList(apiKeyList)
                .timeout(900)
                .proxy(proxy)
                .apiHost(host) //代理地址,https://api.openai.com/=》https://api.f2gpt.com
                .build()
                .init();

        return chatGPT;
    }
}
