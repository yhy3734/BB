package com.eova.cache;

import com.jfinal.plugin.activerecord.cache.ICache;
import com.jfinal.plugin.ehcache.CacheKit;
import lombok.Builder;

/**
 * <p>Project: bb-project - CacheNone</p>
 * <p>描述：DB无cache方案</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/7 14:03
 * @Version 1.0
 * @since 8
 */
@Builder
public class CacheNone implements ICache {

    public <T>T get(String cacheName, Object key) {
        return null;
    }

    public void put(String cacheName, Object key, Object value) {

    }

    public void remove(String cacheName, Object key) {
    }

    public void removeAll(String cacheName) {
    }

}
