package com.eova.qywx.callback.impl;

import com.eova.model.AiRobot;
import com.eova.qywx.callback.CreateCustomGroupCallBack;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;

/**
 * <p>Project: jd-aigc - CreateCustomGroupCallBackImpl</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/14 17:46
 * @Version 1.0
 * @since 8
 */
@Slf4j
@RequiredArgsConstructor(staticName = "newInstance", access = AccessLevel.PUBLIC, onConstructor_ = {@Deprecated})
public class CreateCustomGroupCallBackImpl implements CreateCustomGroupCallBack {

    /**
     * 判断群是否存在（存在的话（最好 创建企业微信群中 才认为存在），后续会调用更新接口推送真的qywxId）
     *
     * @param groupDbId
     * @return
     */
    @Override
    public boolean isGroupExists(Long groupDbId) {
        AiRobot robot = AiRobot.dao.getValidRobot(groupDbId);
        if(robot != null && robot.getInt("qywx_state").equals(2))
            return true;
        return false;
    }

    /**
     * 更新企业微信ID
     * 更新完成会后 算是一条完整的群数据
     *
     * @param groupDbId
     * @param qywxGroupId
     * @return
     */
    @Override
    public int updateQywxGroupId(Long groupDbId, String qywxGroupId) {
        AiRobot robot = AiRobot.dao.getValidRobot(groupDbId);
        if(robot != null) {
            robot.set("qywx_id",qywxGroupId);
            robot.set("qywx_state",1);
            robot.update();
            return 1;
        }
        return 0;
    }
}
