package com.eova.qywx.config;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.eova.common.utils.xx;
import com.jfinal.plugin.IPlugin;

import java.util.List;

/**
 * <p>Project: bb-project - QywxPlugin</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/15 12:15
 * @Version 1.0
 * @since 8
 */
public class QywxPlugin implements IPlugin {

    public boolean start() {

        QywxProperties properties = QywxProperties.getInstance();

        String qywx_corpid = xx.getConfig("qywx_corpid");
        String qywx_corpsecret = xx.getConfig("qywx_corpsecret");
        String qywx_wxbase = xx.getConfig("qywx_wxbase");
        String qywx_wtbase = xx.getConfig("qywx_wtbase");
        int qywx_tokentime = xx.getConfigInt("qywx_tokentime",0);
        int qywx_qrcodevaliddays = xx.getConfigInt("qywx_qrcodevaliddays",0);
        String qywx_robotwithwxaccs = xx.getConfig("qywx_robotwithwxaccs");


        properties.setCorpid(StrUtil.isNotEmpty(qywx_corpid) ? qywx_corpid : null);
        properties.setCorpsecret(StrUtil.isNotEmpty(qywx_corpsecret) ? qywx_corpsecret : null);
        properties.setWxBase(StrUtil.isNotEmpty(qywx_wxbase) ? qywx_wxbase : null);
        properties.setWtBase(StrUtil.isNotEmpty(qywx_wtbase) ? qywx_wtbase : null);


        if(qywx_tokentime > 0)
            properties.setTokenTime(qywx_tokentime);
        if(qywx_qrcodevaliddays > 0)
            properties.setQrcodeValidDays(qywx_qrcodevaliddays);

        if(StrUtil.isNotEmpty(qywx_robotwithwxaccs)){
            List<QywxProperties.RobotWithWxAcc> robotWithWxAccs = JSON.parseArray(qywx_robotwithwxaccs, QywxProperties.RobotWithWxAcc.class);
            if( CollUtil.isNotEmpty(robotWithWxAccs) ){
                properties.setRobotWithWxAccs(robotWithWxAccs);
            }
        }
        properties.init();
        return true;
    }

    public boolean stop() {
        return true;
    }
}
