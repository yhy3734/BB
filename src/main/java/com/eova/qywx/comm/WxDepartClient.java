package com.eova.qywx.comm;

import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.Var;
import com.eova.qywx.comm.vo.*;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WxDepartClient</p>
 * <p>描述：企业部门相关接口
 * https://developer.work.weixin.qq.com/document/path/90245
 * </p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 10:40
 * @Version 1.0
 * @since 8
 */
public interface WxDepartClient {

    /**
     * 获取子部门ID列表
     * @param id 为空则全部
     * @param accessToken
     * @return
     */
    @Get(url = "#{wxBase}/cgi-bin/department/simplelist?access_token={accessToken}&id={id}")
    DepartResultVo simplelist(@Var("accessToken") String accessToken, @Var("id") String id);


}
