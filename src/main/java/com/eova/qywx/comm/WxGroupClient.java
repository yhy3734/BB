package com.eova.qywx.comm;

import com.dtflys.forest.annotation.*;
import com.eova.qywx.comm.vo.*;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WxGroupClient</p>
 * <p>描述：企业群相关接口
 * https://developer.work.weixin.qq.com/document/path/90245
 * </p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 10:40
 * @Version 1.0
 * @since 8
 */
public interface WxGroupClient {

    /**
     * 创建群（内部）
     * @param req
     * @param accessToken
     * @return
     */
    @Post(url = "${wxBase}/cgi-bin/appchat/create?access_token={accessToken}", contentType = "application/json")
    @BodyType(type = "json")
    GroupCreateResultVo create(@Body GroupCreateVo req,@Var("accessToken") String accessToken );

    /**
     * 修改群（内部）
     * @param req
     * @param accessToken
     * @return
     */
    @Post(url = "${wxBase}/cgi-bin/appchat/update?access_token={accessToken}", contentType = "application/json")
    @BodyType(type = "json")
    BaseResponse update(@Body GroupUpdateVo req, @Var("accessToken") String accessToken);

    /**
     * 群信息（内部）
     * @param chatid
     * @param accessToken
     * @return
     */
    @Get(url = "${wxBase}/cgi-bin/appchat/get?access_token={accessToken}&chatid={chatid}", contentType = "application/json")
    GroupInfoResultVo info(@Var("chatid") String chatid, @Var("accessToken") String accessToken);


    /**
     * 获取客户群列表
     * @param req
     * @param accessToken
     * @return
     */
    @Post(url = "${wxBase}/cgi-bin/externalcontact/groupchat/list?access_token={accessToken}", contentType = "application/json")
    CustomGroupListResultVo list(@Body  CustomGroupListVo req, @Var("accessToken") String accessToken);

    /**
     * 获取客户群列表
     * @param req
     * @param accessToken
     * @return
     */
    @Post(url = "${wxBase}/cgi-bin/externalcontact/groupchat/get?access_token={accessToken}", contentType = "application/json")
    CustomGroupInfoResultVo groupInfo(@Body  CustomGroupInfoVo req, @Var("accessToken") String accessToken);


    /**
     * 客户群「加入群聊」管理
     * @param req
     * @param accessToken
     * @return
     */
    @Post(url = "${wxBase}/cgi-bin/externalcontact/groupchat/add_join_way?access_token={accessToken}", contentType = "application/json")
    CustomGroupToJoinResultVo toJoin(@Body  CustomGroupToJoinVo req, @Var("accessToken") String accessToken);

    /**
     * 获取客户群进群方式配置
     * @param req
     * @param accessToken
     * @return
     */
    @Post(url = "${wxBase}/cgi-bin/externalcontact/groupchat/get_join_way?access_token={accessToken}", contentType = "application/json")
    CustomGroupJoinInfoResultVo joinInfo(@Body  CustomGroupJoinInfoVo req, @Var("accessToken") String accessToken);


}
