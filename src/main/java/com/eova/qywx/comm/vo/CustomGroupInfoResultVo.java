package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - CustomGroupInfoResultVo</p>
 * <p>描述：获取客户群详情 查询结果</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 18:43
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class CustomGroupInfoResultVo extends BaseResponse{

    /**
     * 客户群列表
     */
    private GroupChat group_chat;



    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class GroupChat{

        /**
         * 客户群ID
         */
        private String chat_id;

        /**
         *群名
         */
        private String name;

        /**
         *群主ID
         */
        private String owner;

        /**
         *群的创建时间
         */
        private Long create_time;

        /**
         *群公告
         */
        private String notice;

        /**
         *群成员列表
         */
        private List<Member> member_list;

        /**
         *群管理员列表
         */
        private List<Admin>  admin_list;

    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class Member{

        /**
         *群成员id
         */
        private String userid;

        /**
         *成员类型。
         * 1 - 企业成员
         * 2 - 外部联系人
         */
        private Integer type;

        /**
         * 外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。
         * 仅当群成员类型是微信用户（包括企业成员未添加好友），
         * 且企业绑定了微信开发者ID有此字段（查看绑定方法）。
         * 第三方不可获取，上游企业不可获取下游企业客户的unionid字段
         */
        private String unionid;

        /**
         *入群时间
         */
        private Long join_time;

        /**
         *入群方式。
         * 1 - 由群成员邀请入群（直接邀请入群）
         * 2 - 由群成员邀请入群（通过邀请链接入群）
         * 3 - 通过扫描群二维码入群
         */
        private Integer join_scene;

        /**
         *邀请者。目前仅当是由本企业内部成员邀请入群时会返回该值
         */
        private Invitor invitor;

        /**
         *在群里的昵称
         */
        private String group_nickname;

        /**
         *字。仅当 need_name = 1 时返回
         * 如果是微信用户，则返回其在微信中设置的名字
         * 如果是企业微信联系人，则返回其设置对外展示的别名或实名
         */
        private String name;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class Admin{

        /**
         * 群管理员userid
         */
        private String userid;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public  static class Invitor{

        /**
         * 邀请者的userid
         */
        private String userid;
    }
}
