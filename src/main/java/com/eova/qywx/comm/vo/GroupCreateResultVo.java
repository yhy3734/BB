package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - GroupCreateResultVo</p>
 * <p>描述：企业群创建结果</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 11:00
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class GroupCreateResultVo extends BaseResponse{

    /**
     * 群聊的唯一标志
     */
    private String chatid;
}
