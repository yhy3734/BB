package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WTResponseVo</p>
 * <p>描述：企微WorkTool API返回响应</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 16:20
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class WTBackResponseVo {
    /**
     * 0 调用成功 -1或其他值 调用失败并回复message
     */
    @Builder.Default
    private Integer code = 0;

    /**
     * 对本次接口调用的信息描述
     */
    @Builder.Default
    private String message = "成功";

    /**
     * 返回数据
     */
    private ResponseData data;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class ResponseData{
        /**
         * 5000 回答类型为文本
         */
        private Integer type;

        /**
         * 回答结果集合
         */
        private ResponseDataInfo info;
    }


    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class ResponseDataInfo{
        /**
         * 响应内容，如果不想回复内容,本初设置为“”
         */
        private String text;

    }

}
