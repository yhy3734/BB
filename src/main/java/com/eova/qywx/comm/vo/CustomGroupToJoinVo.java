package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - CustomGroupToJoinVo</p>
 * <p>描述：客户群「加入群聊」to加入参数</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 18:51
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class CustomGroupToJoinVo {

    /**
     * 场景。
     * 1 - 群的小程序插件
     * 2 - 群的二维码插件
     */
    @Builder.Default
    private Integer scene = 2;

    /**
     *联系方式的备注信息，用于助记，超过30个字符将被截断
     */
    private String remark;

    /**
     *当群满了后，是否自动新建群。0-否；1-是。 默认为1
     */
    @Builder.Default
    private Integer auto_create_room = 0;

    /**
     * 	使用该配置的客户群ID列表，支持5个
     */
    private List<String> chat_id_list;

    /**
     * 企业自定义的state参数，用于区分不同的入群渠道。不超过30个UTF-8字符(暂时不用)
     */
    private Integer state;
}
