package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WTMessageReceiveVo</p>
 * <p>描述：业务微信收到的消息(文本)</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 16:13
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class WTMessageReceiveVo {
    /**
     *问题文本
     */
    String spoken;

    /**
     *原始问题文本"@me 你好啊"
     */
    String rawSpoken;

    /**
     *提问者名称
     */
    String receivedName;

    /**
     *QA所在群名（群聊）
     */
    String groupName;

    /**
     *QA所在群备注名（群聊）
     */
    String groupRemark;

    /**
     *QA所在房间类型 1=外部群 2=外部联系人 3=内部群 4=内部联系人
     */
    Integer roomType;

    /**
     *是否@机器人（群聊）
     */
    Boolean atMe;

    /**
     *消息类型 0=未知 1=文本 2=图片 5=视频 7=小程序 8=链接 9=文件
     */
    Integer textType;
}
