package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WTMessageReceiveVo</p>
 * <p>描述：wt收到的回调消息(文本)</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 16:13
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class WTCallBackReceiveVo {
    /**
     *消息id
     */
    String messageId;

    /**
     * 错误码 0为成功 其他为失败
     */
    String errorCode;

    /**
     *错误原因
     */
    String errorReason;

    /**
     *执行时间 执行时间戳(毫秒)
     */
    Long runTime;

    /**
     *指令执行耗时
     */
    Long timeCost;
    /**
     *指令类型
     */
    Integer type;

    /**
     *原始指令
     */
    String rawMsg;

    /**
     *成功名单
     */
    List<String> successList;

    /**
     * 失败名单
     */
    List<String> failList;

    /**
     *成功时不提供
     */
    String groupName;


    /**
     *群二维码链接
     */
    String qrCode;



}
