package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - DepartResultVo</p>
 * <p>描述：部门简单返回对象</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 14:02
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class DepartResultVo extends BaseResponse{

    /**
     * 部门列表数据。
     */
    private List<Department> department;

    public static class Department{

        private Integer id;

        private String name;

        //private  String name_en;

        /**
         * 部门负责人的UserID；第三方仅通讯录应用可获取
         */
        private List<String> department_leader;

        /**
         * 父部门id。根部门为1
         */
        private Integer parentid;

        /**
         *
         */
        private Integer order;

    }
}
