package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - GroupUpdateResultVo</p>
 * <p>描述：企业群修改结果</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 11:24
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class GroupInfoResultVo extends BaseResponse{

    /**
     * 群信息
     */
    private ChatInfo chat_info;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class ChatInfo{

        /**
         * 群聊唯一标志
         */
        private String chatid;

        /**
         * 群聊名
         */
        private String name;

        /**
         * 群聊名
         */
        private String owner;

        /**
         * 群成员id列表
         */
        private List<String> userlist;
    }
}
