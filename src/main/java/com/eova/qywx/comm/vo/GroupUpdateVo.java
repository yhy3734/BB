package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - GroupUpdateVo</p>
 * <p>描述：企业群修改</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 11:21
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class GroupUpdateVo {

    /**
     *群聊id
     */
    private String chatid;

    /**
     * 新的群聊名。若不需更新，请忽略此参数。最多50个utf8字符，超过将截断
     */
    private String name;

    /**
     *新群主的id。若不需更新，请忽略此参数。课程群聊群主必须在设置的群主列表内
     */
    private String owner;

    /**
     *添加成员的id列表
     */
    private List<String> add_user_list;

    /**
     * 踢出成员的id列表
     */
    private List<String> del_user_list;
}
