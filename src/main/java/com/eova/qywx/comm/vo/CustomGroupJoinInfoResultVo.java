package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - CustomGroupJoinInfoResultVo</p>
 * <p>描述：进群方式配置 查询响应</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 18:58
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class CustomGroupJoinInfoResultVo extends BaseResponse{

    /**
     * 配置详情
     */
    private JoinWay join_way;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class JoinWay{

        /**
         * 新增联系方式的配置id
         */
        String config_id;

        /**
         *场景。
         * 1 - 群的小程序插件
         * 2 - 群的二维码插件
         */
        Integer scene;

        /**
         *联系方式的备注信息，用于助记，超过30个字符将被截断
         */
        String remark;

        /**
         *当群满了后，是否自动新建群。0-否；1-是。 默认为1
         */
        Integer auto_create_room;

        /**
         *自动建群的群名前缀，当auto_create_room为1时有效。最长40个utf8字符
         */
        String room_base_name;

        /**
         *自动建群的群起始序号，当auto_create_room为1时有效
         */
        Integer room_base_id;

        /**
         *使用该配置的客户群ID列表。
         */
        List<String> chat_id_list;

        /**
         *	联系二维码的URL或小程序插件的URL
         */
        String qr_code;


        /**
         *业自定义的state参数，用于区分不同的入群渠道。不超过30个UTF-8字符
         * 如果有设置此参数，在调用获取客户群详情接口时会返回每个群成员对应的该参数值，
         */
        String state;

    }
}
