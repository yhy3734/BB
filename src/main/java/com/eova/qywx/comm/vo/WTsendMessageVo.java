package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WTsendMessageVo</p>
 * <p>描述：发送消息参数</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 17:17
 * @Version 1.0
 * @since 8
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder
public class WTsendMessageVo {
    /**
     * 未知，固定2
     */
    @Builder.Default
    private Integer socketType = 2;

    /**
     * 多个消息（支持多个）
     */
    private List<WTsendOneMessageVo> list;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class WTsendOneMessageVo{

        /**
         * 203-固定值
         */
        @Builder.Default
        private Integer type = 203;

        /**
         * 昵称或群名
         */
        private List<String> titleList;

        /**
         * 发送文本内容 (\n换行)
         */
        private String receivedContent;

        /**
         * at的人(at所有人用"@所有人")
         */
        private List<String> atList;

    }
}
