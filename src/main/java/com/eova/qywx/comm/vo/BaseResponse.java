package com.eova.qywx.comm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - BaseResponse</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/12 10:58
 * @Version 1.0
 * @since 8
 */
@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse {

    /**
     * 返回码 0=成功
     */
    Integer errcode;

    /**
     * 对返回码的文本描述内容
     */
    String errmsg;


}
