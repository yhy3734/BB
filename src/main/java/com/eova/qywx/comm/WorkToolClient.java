package com.eova.qywx.comm;

import com.dtflys.forest.annotation.Body;
import com.dtflys.forest.annotation.BodyType;
import com.dtflys.forest.annotation.Post;
import com.dtflys.forest.annotation.Var;
import com.eova.qywx.comm.vo.*;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - WorkToolClient</p>
 * <p>描述：企微WorkTool接口
 * 见：https://apifox.com/apidoc/project-1035094/api-23520350
 * </p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/13 17:11
 * @Version 1.0
 * @since 8
 */
public interface WorkToolClient {

    /**
     * 创建外部群
     * @param req
     */
    @Post(url = "${wtBase}/wework/sendRawMessage?robotId={robotId}", contentType = "application/json")
    @BodyType(type = "json")
    WTResponseVo createGroup(@Body WTCreateGroupVo req, @Var("robotId") String robotId);


    /**
     * 发消息
     * @param req
     * @param robotId
     * @return
     */
    @Post(url = "${wtBase}/wework/sendRawMessage?robotId={robotId}", contentType = "application/json")
    @BodyType(type = "json")
    WTResponseVo sendMessage(@Body WTsendMessageVo req, @Var("robotId") String robotId);


}
