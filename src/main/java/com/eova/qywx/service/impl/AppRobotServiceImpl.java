package com.eova.qywx.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.eova.exception.BusinessException;
import com.eova.model.AiRobot;
import com.eova.model.User;
import com.eova.qywx.config.QywxProperties;
import com.eova.qywx.service.AppRobotService;
import com.eova.qywx.service.CustomGroupService;
import com.google.common.collect.Lists;
import com.eova.qywx.comm.WorkToolClient;
import com.eova.qywx.comm.vo.*;
import com.eova.qywx.service.QyWxGroupService;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>Project: ChatGPT-on-Wechat-Business-master - AppRobotService</p>
 * <p>描述：WorkTool 通讯服务，主要是建群和发消息
 * https://apifox.com/apidoc/project-1035094/api-23520034
 * 错误码：1=成功，0=失败，2=已提交
 * </p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/14 8:28
 * @Version 1.0
 * @since 8
 */
@RequiredArgsConstructor(staticName = "newInstance", access = AccessLevel.PUBLIC, onConstructor_ = {@Deprecated})
@Slf4j
public class AppRobotServiceImpl implements AppRobotService {

    private final WorkToolClient workToolClient;
    private final QyWxGroupService qyWxGroupService;
    private final QywxProperties qywxProperties;




    /**
     * 建群，形成一个 groupName+“_”+groupOrder 的群，群主为 ownerNickName
     * @param groupName 群名，比如：鲁迅
     * @param groupOrder 系统编号，识别此群的
     * @param ownerNickName 群主昵称（机器）
     * @param robotId
     * @return 1=成功，0=失败，2=已提交
     */
    @Override
    public Pair<Integer,String> createGroup(@NonNull String groupName, @NonNull Long groupOrder, @NonNull String ownerNickName, @NonNull String robotId){
        String finalGroupName = qyWxGroupService.getGroupName(groupName,groupOrder);
        List<WTCreateGroupVo.WTCreateOneGroupVo> oneGroupVos =
                Lists.newArrayList(
                        WTCreateGroupVo.WTCreateOneGroupVo
                                .builder()
                                .groupName(finalGroupName)
                                .groupRemark(finalGroupName)
                                .selectList(Lists.newArrayList(ownerNickName))
                                .build()
                );

        int ret = createGroup(oneGroupVos,robotId);

        return Pair.of(ret,finalGroupName);
    }


    public CreateCustomGroupResponse create(String name, Long id){
        QywxProperties.RobotWithWxAcc robotWithWxAcc = RandomUtil.randomEle(qywxProperties.getRobotWithWxAccs());

        String ownerName =  RandomUtil.randomEle(robotWithWxAcc.getWxNames());
        return create(name,id,ownerName,robotWithWxAcc.getRobotId());
    }

    public CreateCustomGroupResponse create(@NonNull String name, @NonNull Long id, @NonNull String ownerName, @NonNull String theRobotId){

        String theGroupName = qyWxGroupService.getGroupName(name,id);

        String groupId = qyWxGroupService.groupIdByName(theGroupName);
        if(StrUtil.isNotEmpty(groupId)){
            return CreateCustomGroupResponse
                    .builder()
                    .state(1)
                    .groupName(theGroupName)
                    .groupId(groupId)
                    .robotId(theRobotId)
                    .build();
        }
        Pair<Integer,String> ret = createGroup(name,id,ownerName,theRobotId);
        if (ret.getKey() == 1 || ret.getKey() == 2){
            //写入db 或者db修正成已创建中
            return CreateCustomGroupResponse
                    .builder()
                    .state(2)
                    .groupName(theGroupName)
                    .robotId(theRobotId)
                    .build();
        }
        return  CreateCustomGroupResponse
                .builder()
                .state(0)
                .groupName(theGroupName)
                .robotId(theRobotId)
                .build();
    }

    /**
     * 发送一条消息
     * @param to 用户昵称 或 群名
     * @param sendContent 发送内容(\n换行)
     * @param atList at的人(at所有人用"@所有人")，群才需要
     * @param robotId
     * @return 1=成功，0=失败，2=已提交
     */
    @Override
    public int sendMessage(@NonNull String to, @NonNull String sendContent, List<String> atList, String robotId){
        WTsendMessageVo.WTsendOneMessageVo oneMessageVo = WTsendMessageVo.WTsendOneMessageVo
                .builder()
                .titleList(Lists.newArrayList(to))
                .receivedContent(sendContent)
                .atList(atList)
                .build();

        return sendMessage(Lists.newArrayList(oneMessageVo), robotId);
    }

    /**
     * 发送消息
     * @param msgs
     * @param robotId
     * @return 1=成功，0=失败 2=提交成功
     */
    @Override
    public int sendMessage(List<WTsendMessageVo.WTsendOneMessageVo> msgs, String robotId){
        WTsendMessageVo req = WTsendMessageVo
                .builder()
                .list(msgs)
                .build();
        WTResponseVo responseVo = workToolClient.sendMessage(req, robotId);
        if (!responseVo.getCode().equals(0)){
            log.warn("发送消息：{} 失败：{}（{}）", JSONUtil.toJsonStr(msgs),responseVo.getMessage(),responseVo.getCode());
        }
        return code2LocalRet(responseVo.getCode());
    }


    /**
     * 建群（建议用上面接口）
     * @param oneGroupVos
     * @param robotId
     * @return 1=成功，0=失败，2=已提交
     */
    @Override
    public int createGroup(List<WTCreateGroupVo.WTCreateOneGroupVo> oneGroupVos, String robotId){
        WTCreateGroupVo req = WTCreateGroupVo
                .builder()
                .list(oneGroupVos)
                .build();
        WTResponseVo responseVo = workToolClient.createGroup(req, robotId);
        if (!responseVo.getCode().equals(0)){
            log.warn("创建群：{} 失败：{}（{}）", JSONUtil.toJsonStr(oneGroupVos),responseVo.getMessage(),responseVo.getCode());
        }
        return code2LocalRet(responseVo.getCode());
    }


    public AiRobot sbumitQywx(@NonNull Long robotId, User user){
        AiRobot robot = AiRobot.dao.getValidRobot(robotId);
        if(robot == null || !robot.getLong("user_id").equals(user.getId().longValue()))
            throw new BusinessException("无此机器人信息");

        //微信客服状态：0=初始,1=ok,2=处理中
        if( Lists.newArrayList(0,2).contains(robot.getInt("qywx_state")) ){//未开，先开
            CreateCustomGroupResponse response = create(robot.getStr("name"),robotId);
            if(response.getState() == 1 || response.getState() == 2){
                robot.set("qywx_state",response.getState());
                robot.set("qywx_id",response.getGroupId());
                robot.set("qywx_related_robotid",response.getRobotId());
                robot.set("qywx_name",response.getGroupName());
            }else {
                robot.set("qywx_state",0);
            }
            robot.set("update_time",new Date());
            robot.update();
        }

        if(robot.getInt("qywx_state").equals(1)){//已经创建好群了，提取

            Pair<String, Date> qrcode = getTheQrcode(robot);
            robot.set("qywx_qr_code",null);
            robot.set("qywx_qr_code_valid_day",null);
            if (qrcode != null){
                robot.set("qywx_qr_code",qrcode.getKey());
                robot.set("qywx_qr_code_valid_day",qrcode.getRight());
                robot.update();
            }
        }
        return robot;
    }


    public void getQrCodeAndSave(AiRobot robot){
        if(robot != null && robot.getInt("qywx_state").equals(1)) {//已经创建好群了，提取
            Pair<String, Date>  qrcode = getTheQrcode(robot);
            robot.set("qywx_qr_code",null);
            robot.set("qywx_qr_code_valid_day",null);
            if (qrcode != null){
                robot.set("qywx_qr_code",qrcode.getKey());
                robot.set("qywx_qr_code_valid_day",qrcode.getRight());
                robot.update();
            }
        }
    }

    /**
     * 默认有效先设定为7天吧（确保微信群已建立）
     * @param robot
     * @return <qrcode,有效截止日>
     */
    private Pair<String, Date> getTheQrcode(AiRobot robot){
        Date now = new Date();

        if(StrUtil.isNotEmpty( robot.getStr("qywx_qr_code") ) && robot.getDate("qywx_qr_code_valid_day").compareTo(now)>0){
            return Pair.of(robot.getStr("qywx_qr_code"),robot.getDate("qywx_qr_code_valid_day")) ;
        }else{
            CustomGroupService customGroupService = CustomGroupService.getCustomGroupService();
            String configId = customGroupService.toJoin( robot.getStr("qywx_id") );
            if(StrUtil.isEmpty(configId) ){
                return null;
            }
            CustomGroupJoinInfoResultVo.JoinWay joinWay = customGroupService.joinInfo(configId);
            if(joinWay != null){
                return Pair.of(joinWay.getQr_code(), DateUtil.offsetDay(now,customGroupService.getQrcodeValidDays())) ;
            }
        }
        return null;
    }


    /**
     * 远程码转本地码
     * 0-》1
     * 200-》2
     * 其他-》0
     * @param code
     * @return 1=成功，0=失败，2=已提交
     */
    private  int code2LocalRet(Integer code){
        if(code.equals(0))
            return 1;
        else if(code.equals(200))
            return 2;
        else
            return 0;
    }

}
