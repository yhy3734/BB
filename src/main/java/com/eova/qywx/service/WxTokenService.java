package com.eova.qywx.service;

import com.dtflys.forest.Forest;
import com.eova.qywx.comm.TokenClient;
import com.eova.qywx.comm.WxGroupClient;
import com.eova.qywx.config.QywxProperties;
import com.eova.qywx.service.impl.CustomGroupServiceImpl;
import com.eova.qywx.service.impl.WxTokenServiceImpl;

/**
 * <p>Project: jd-aigc - WxTokenService</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/14 15:46
 * @Version 1.0
 * @since 8
 */
public interface WxTokenService {

    public static WxTokenService getWxTokenService(){
        TokenClient tokenClient = Forest.client(TokenClient.class);
        QywxProperties qywxProperties = QywxProperties.getInstance();

        return WxTokenServiceImpl.newInstance(tokenClient,qywxProperties);
    }

    /**
     * 获取token（可以缓存 120分钟）
     * @return
     */
    String getToken();

    /**
     * 业务提取token（）
     * @return
     */
    String getTokenWithErr();
}
