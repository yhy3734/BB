package com.eova.qywx.service;

import cn.hutool.core.util.RandomUtil;
import com.dtflys.forest.Forest;
import com.eova.model.AiRobot;
import com.eova.model.User;
import com.eova.qywx.comm.WorkToolClient;
import com.eova.qywx.comm.vo.WTCreateGroupVo;
import com.eova.qywx.comm.vo.WTsendMessageVo;
import com.eova.qywx.config.QywxProperties;
import com.eova.qywx.service.impl.AppRobotServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

/**
 * <p>Project: jd-aigc - AppRobotService</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/14 15:44
 * @Version 1.0
 * @since 8
 */
public interface AppRobotService {

    public static AppRobotService getAppRobotService(){
        WorkToolClient workToolClient = Forest.client(WorkToolClient.class);
        QyWxGroupService qyWxGroupService = QyWxGroupService.getQyWxGroupService();
        QywxProperties qywxProperties = QywxProperties.getInstance();

        return AppRobotServiceImpl.newInstance(workToolClient,qyWxGroupService,qywxProperties);
    }

    /**
     * 创建群（机器人以及账户随机）
     * @param name
     * @param id
     * @return
     */
    CreateCustomGroupResponse create(String name, Long id);

    /**
     * 创建群
     * 1、db写入一条群
     * 2、调用接口创建（异步的）
     * 3、通过机器人回调接口 通知群建好
     * 4、查询企业微信数据，查询出id以及二维码
     * @param name
     * @param id
     * @param ownerName
     * @param theRobotId
     * @param id <结果,群名称，群ID> 结果（1=成功，2=创建中，其他为失败）,群名称,群ID
     */
    CreateCustomGroupResponse create(String name, Long id, String ownerName, String theRobotId);

    Pair<Integer, String> createGroup(@NonNull String groupName, @NonNull Long groupOrder, @NonNull String ownerNickName, @NonNull String robotId);

    /**
     * 推送微信消息
     * @param to
     * @param sendContent 目前推送的@xx 文本内容和@xx会连接一块，需要价格空格" ",请自行添加，本函数不会添加
     * @param atList
     * @param robotId
     * @return
     */
    int sendMessage(@NonNull String to, @NonNull String sendContent, List<String> atList, String robotId);

    int sendMessage(List<WTsendMessageVo.WTsendOneMessageVo> msgs, String robotId);

    int createGroup(List<WTCreateGroupVo.WTCreateOneGroupVo> oneGroupVos, String robotId);


    /**
     * 提交申请开通企业微信群聊（尝试一撸到底，可能会停滞因为申请开通群聊 是异步的）
     * @param robotId
     * @param user
     * @return
     */
    AiRobot sbumitQywx(@NonNull Long robotId, User user);

    /**
     * 获取群qrcode(必须是群开通了)
     * @param robot
     */
    void getQrCodeAndSave(AiRobot robot);

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    @SuperBuilder
    public static class CreateCustomGroupResponse {
        /**
         * 结果1=完成，0=失败，2=处理中
         */
        int state;

        /**
         * 群名
         */
        String groupName;
        /**
         * 群id
         */
        String groupId;

        /**
         * 关联的机器人
         */
        String robotId;
    }
    
}
