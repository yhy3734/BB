package com.eova.qywx.service;

import com.dtflys.forest.Forest;
import com.eova.qywx.callback.CreateCustomGroupCallBack;
import com.eova.qywx.comm.WorkToolClient;
import com.eova.qywx.comm.WxGroupClient;
import com.eova.qywx.comm.vo.WTCallBackReceiveVo;
import com.eova.qywx.config.QywxProperties;
import com.eova.qywx.service.impl.AppRobotServiceImpl;
import com.eova.qywx.service.impl.CustomGroupServiceImpl;
import com.eova.qywx.service.impl.QyWxGroupServiceImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.tuple.Pair;

/**
 * <p>Project: jd-aigc - QyWxGroupService</p>
 * <p>描述：</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/14 15:45
 * @Version 1.0
 * @since 8
 */
public interface QyWxGroupService {


    public static QyWxGroupService getQyWxGroupService(){
        CustomGroupService customGroupService = CustomGroupService.getCustomGroupService();
        QywxProperties qywxProperties = QywxProperties.getInstance();
        CreateCustomGroupCallBack createCustomGroupCallBack = CreateCustomGroupCallBack.getCreateCustomGroupCallBack();

        return QyWxGroupServiceImpl.newInstance(customGroupService,qywxProperties,createCustomGroupCallBack);
    }


    String getGroupName(String name, Long groupOrder);

    /**
     * 根据groupName，提取到groupId
     * @param groupName 格式 xx_numberId
     * @return <id,名称></>
     */
    Pair<Long,String> groupIdByAnalysisGroupName(String groupName);



    void onGroupCreateReceived(WTCallBackReceiveVo param);

    String groupIdByName(String theGroupName);


}
