/**
 * @Copyright (C)
 * @Description:
 */
package com.eova.qywx;

import cn.bblocks.chatgpt.entity.chat.ChatCompletionResponse;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.eova.ai.vo.ChatSession;
import com.eova.common.Response;
import com.eova.config.EovaConfig;
import com.eova.qywx.service.AppRobotService;
import com.eova.qywx.service.QyWxGroupService;
import com.eova.service.sm;
import com.google.common.collect.Lists;
import com.jfinal.core.Controller;
import com.eova.qywx.comm.vo.*;
import com.eova.qywx.config.QywxProperties;
import com.eova.qywx.service.impl.AppRobotServiceImpl;
import com.eova.qywx.service.impl.QyWxGroupServiceImpl;
import com.jfinal.plugin.redis.Redis;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description: 机器人回调(新的消息，以及群的通知)
 * @Zhao [125043150@qq.com]
 * @CreateTime: 2023/3/2 17:01
 */
@Slf4j
/*@RestController
@RequestMapping("/qywx")*/
public class QyWxController extends Controller {
    public static final String MODULE = "QYWX";
    /*private AppRobotService appRobotService = AppRobotService.getAppRobotService();
    private QyWxGroupService qyWxGroupService = QyWxGroupService.getQyWxGroupService();
    private QywxProperties qywxProperties = QywxProperties.getInstance();;*/

    /**
     * 一条空消息
     */
    private static WTBackResponseVo empty =  WTBackResponseVo
            .builder()
            .data(
                WTBackResponseVo.ResponseData
                                    .builder()
                                    .type(5000)
                                    .info(WTBackResponseVo.ResponseDataInfo
                                                  .builder()
                                            .text("")
                                            .build())
                                       .build()
                    )
            .build();


    /**
     * 收到微信的消息
     */
    public void msgReceived()  {
        String robotId = this.get(0);
        WTMessageReceiveVo param = JSON.parseObject(this.getRawData(),WTMessageReceiveVo.class);

        log.info("robotId：{} msgReceived:{}",robotId, JSONUtil.toJsonStr(param));
        if (Integer.valueOf(1).equals(param.getTextType())){
            if(param.getAtMe()!=null && param.getAtMe()){
                String spoken = param.getSpoken();//问题文本
                String groupName = param.getGroupName();//
                String receivedName = param.getReceivedName();//提问者名称

                Pair<Long,String> groupPair = QyWxGroupService.getQyWxGroupService().groupIdByAnalysisGroupName(groupName);
                if(groupPair == null) {
                    log.info("此群：{} 未匹配到本地机器人",groupName);
                    renderJson(empty);
                    return;
                }

                Long localRobotId = groupPair.getKey();
                Runnable runnable = ()->{
                    String key = EovaConfig.SYS_MODULE +"::"+ MODULE + "::"+robotId;
                    ChatSession chatSession = Redis.use().get(key);
                    try{
                        chatSession = sm.chatService.getWxChat(localRobotId);
                    }catch (Exception e){
                        log.info("wxchat异步机器人：{}，执行异常不执行回复：",localRobotId,e);
                        return;
                    }
                    ChatCompletionResponse completionResponse = sm.chatService.chatByChatSession(chatSession,spoken);
                    String sendContent = completionResponse.getFinalContent();
                    String to = groupName;
                    List<String> atList = Lists.newArrayList(receivedName);
                    /**
                     * 内容价格空格，以让@xx 和 内容中间分隔
                     */
                    AppRobotService.getAppRobotService().sendMessage(to," "+sendContent,atList,robotId);
                };
                log.info("机器人：{} 开始异步回复答案。。。",localRobotId);
                ThreadUtil.execAsync(runnable,true);
                renderJson( empty );

                /*String sendContent = "你好啊,你的问题："+spoken+"，gpt还未对完完成！（同步）";
                renderJson(
                        WTBackResponseVo
                                .builder()
                                .data(
                                        WTBackResponseVo.ResponseData
                                                .builder()
                                                .type(5000)
                                                .info(WTBackResponseVo.ResponseDataInfo
                                                        .builder()
                                                        .text(sendContent)
                                                        .build())
                                                .build()
                                )
                                .build()
                );*/
            }else
                renderJson(empty);
        }else{
            log.info("非文本消息，暂时不予回复！");
            renderJson( empty);
        }
    }


    /**
     * 回调消息（目前主要监控 建群完成通知 type=206）
     * @return
     */
    public void callBackReceived()  {
        String robotId = this.get(0);
        WTCallBackReceiveVo param = JSON.parseObject(this.getRawData(),WTCallBackReceiveVo.class);
        log.info("robotId：{} callBack receive:{}",robotId, JSONUtil.toJsonStr(param));
        try{
            if (param.getType().equals(206)){//创建群成功
                String groupName = param.getGroupName();
                if(StrUtil.isNotEmpty(groupName) || groupName.indexOf("_") != -1) {
                    log.debug("合规的微信名，将匹配本地数据");
                    QyWxGroupService.getQyWxGroupService().onGroupCreateReceived(param);
                }
            }
        }catch (Exception e){ log.error("Exception:",e);}
        renderJson(Response.suc(""));
    }
}
