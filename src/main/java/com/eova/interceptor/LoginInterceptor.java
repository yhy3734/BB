/**
 * Copyright (c) 2013-2016, Jieven. All rights reserved.
 *
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package com.eova.interceptor;

import java.util.ArrayList;

import cn.dev33.satoken.stp.StpUtil;
import com.eova.common.Response;
import com.eova.common.utils.xx;
import com.eova.common.utils.util.AntPathMatcher;
import com.eova.config.EovaConst;
import com.eova.model.User;
import com.eova.service.sm;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.oss.OSSController;
import lombok.extern.slf4j.Slf4j;

/**
 * 常量加载拦截器
 * 
 * @author Jieven
 * 
 */
@Slf4j
public class LoginInterceptor implements Interceptor {

	/**
	 * 登录拦截排除URI<br>
	 * ?  匹配任何单字符<br> 
	 * *  匹配0或者任意数量的字符<br> 
	 * ** 匹配0或者更多的目录 <br>
	 */
	public static ArrayList<String> excludes = new ArrayList<String>();
	static {
		excludes.add("/captcha");
		excludes.add("/toLogin");
		excludes.add("/vcodeImg");
		excludes.add("/doLogin");
		excludes.add("/initStepOne");
		excludes.add("/initStepTwo");
		excludes.add("/doInit");
		excludes.add("/toTest");
		excludes.add("/form");
		excludes.add("/doForm");
		excludes.add("/upgrade");
		excludes.add("/why");
		//演示地址（齿圈）
		excludes.add("/d3");
		//get 真是IP
		excludes.add("/getIP");
	}

	@Override
	public void intercept(Invocation inv) {
		String uri = inv.getActionKey();

		AntPathMatcher pm = new AntPathMatcher();
		for (String pattern : excludes) {
			if (pm.match(pattern, uri)) {
				inv.invoke();
				return;
			}
		}

		// 获取登录用户的角色
		//User user = inv.getController().getAttr(EovaConst.USER);

		if(!StpUtil.isLogin() ){
			xx.redirect(inv.getController(), Response.NO_LOGIN,"未登录或登录超时。请重新登录", "/toLogin");
			return;
		}else{
			User user = initUserByUserId(StpUtil.getLoginIdAsInt());
			if(user == null){
				xx.redirect(inv.getController(), Response.NO_LOGIN,"未登录或登录超时。请重新登录", "/toLogin");
				return;
			}
			inv.getController().setAttr(EovaConst.USER,user);
		}
		inv.invoke();
	}

	private User initUserByUserId(Integer userId)  {
		User user = (User)StpUtil.getSession().getByDevice(EovaConst.USER);
		if(user == null){
			user = User.dao.getByCache(userId);
			try {
				OSSController index = new OSSController();

				sm.auth.doLoginInitWithSession(user, (u)->{
					try {
						index.loginInit(u);
					}catch (Exception e){
						throw new RuntimeException(e);
					}
				});
				return user;
			}catch (Exception e){
				log.error("初始化用户登录信息失败：",e);
			}
			return null;
		}else{
			return user;
		}
	}
}