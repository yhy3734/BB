package com.eova.model;

import com.eova.common.base.BaseModel;
import com.eova.common.utils.xx;
import com.jfinal.plugin.activerecord.Db;


/**
 * <p>Project: bb-project - AiChatSession</p>
 * <p>描述：ai会话</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/6 9:24
 * @Version 1.0
 * @since 8
 */
public class AiChatSession extends BaseModel<AiChatSession> {
	public static final AiChatSession dao = new AiChatSession();



	public int updateOver(long sessionId){
		return Db.use(xx.DS_EOVA).update("UPDATE ai_chat_session SET end_time=NOW(),update_time=NOW() WHERE id=?", sessionId);
	}


	/**
	 * 查询 type=1微信群专属聊天对话
	 * @param robotId
	 * @return
	 */
	public AiChatSession getWxChatSession(Long robotId){
		return this.findFirst("SELECT * FROM ai_chat_session s WHERE s.robot_id=? AND s.type=2 order by s.id desc limit 1",
				robotId);
	}
}
