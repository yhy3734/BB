package com.eova.model;

import com.eova.common.base.BaseModel;

import java.util.List;


/**
 * <p>Project: bb-project - AiUserKeys</p>
 * <p>描述：ai用户秘钥</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/6 9:24
 * @Version 1.0
 * @since 8
 */
public class AiUserKeys extends BaseModel<AiUserKeys> {
	public static final AiUserKeys dao = new AiUserKeys();

	public List<AiUserKeys> queryUserKeys(Long userId) {
		return this.find("SELECT * FROM ai_user_keys k WHERE k.user_id=? and k.state=1 and k.is_delete=0",
				userId);
	}
}
