package com.eova.model;

import com.eova.common.base.BaseModel;
import com.eova.common.utils.xx;
import com.jfinal.plugin.activerecord.Db;

import java.util.List;


/**
 * <p>Project: bb-project - AiModel</p>
 * <p>描述：ai模型-本地知识库，存入的 id为："id_"+AiModel.id</p>
 *
 * @Author Zhao [125043150@qq.com]
 * @Date 2023/9/6 9:24
 * @Version 1.0
 * @since 8
 */
public class AiModel extends BaseModel<AiModel> {
	public static final AiModel dao = new AiModel();


	public List<AiModel> queryModelByIds(List<String> modelIds) {
		List<AiModel> result = this.find("SELECT * FROM ai_model m WHERE m.id IN(" + xx.join(modelIds.toArray(), ',') + ") AND m.`state`=2"
				);
		return result;
	}


	/**
	 *获取模型向量，
	 * 目前Milvus 要求id不能是数字，所以存入的转为："id_"+AiModel.id
	 * @return
	 */
/*	public String getAiModelVectorId(){
		return  "id_" + this.getLong("id");
	}*/


	/**
	 * 查询待训练模型（含 robot_id）
	 * @param limit
	 * @return
	 */
	public List<AiModel> getToTrainModel(int limit){
		return this.find("select m.*,v.robot_id\n" +
				"from ai_model m inner join\n" +
				"(\n" +
				"SELECT m.id,max(r.`id`) robot_id \n" +
				"FROM ai_model m INNER JOIN ai_robot r ON FIND_IN_SET( m.`id`,r.`model_ids`) AND r.`is_delete`=0 AND r.`state`=1\n" +
				"WHERE  m.`state`=0\n" +
				"GROUP BY m.id\n" +
				")v on m.id=v.id\n" +
				"order by  m.update_time\n" +
				"limit ?",limit);
	}
}
